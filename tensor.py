import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

#Import data from CSV file
train = pd.read_csv('titanic_data/train.csv')

#Analyze the data
print(train.count())
#Check the first five entries
print(train.head())
#Check the missing data using heatmap
sns.heatmap(train.isnull(), yticklabels=False, cbar= False, cmap='viridis')
#Create a function to return average age based on passenger class
def impute_age(col):
    Age = col[0]
    Pclass = col[1]
    
    if pd.isnull(Age):
        if Pclass ==1:
            return 37
        elif Pclass ==2:
            return 29
        else:
            return 24
    else:
        return Age
    
#Fill values
train['Age'] = train[['Age', 'Pclass']].apply(impute_age, axis=1)

#Check the heatmap
sns.heatmap(train.isnull(), yticklabels=False, cbar= False, cmap='viridis')
plt.show()
train.drop(['Cabin', 'Name', 'Ticket'], axis = 1, inplace=True) #inplace = True will not show you the values

#Display first five records
#print(train.head())
#Create a new variable sex with only one column, 1 as male and 0 as female.
sex = pd.get_dummies(train['Sex'], drop_first = True) #drop_fist = True will drop the first column as not relevant

#Create a new variable embark with two column, if both Q and S are zero it means C is 1. 
embark = pd.get_dummies(train['Embarked'], drop_first = True) #first column is not required

#Drop the old column 'Sex' and 'Embarked'
train.drop(['Sex', 'Embarked'], axis=1, inplace=True)

#Create a new data set with quantitative information
train_new = pd.concat([train, sex, embark], axis=1)

#Display first five values
#print(train_new.head())
#import train_test_split function
from sklearn.model_selection import train_test_split

#Create your input and ouput data set
X = train.drop('Survived', axis = 1)
y = train['Survived']

#Divide your data set into train/test set
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.30, random_state = 101)

#Import the Logistic Regression Model and Train/Tet the model
from sklearn.linear_model import LogisticRegression

#Initialize the model to null
logmodel = LogisticRegression()

# Use fit method to train the model
logmodel.fit(X_train, y_train)

#Use predict method to test the model
predictions = logmodel.predict(X_test)

#Import Classification report from sklearn to show the analysis
from sklearn.metrics import classification_report

# Show classification report parameters
print (classification_report(y_test, predictions))
print(predictions)

